'use strict';
module.exports = function(app) {
    var deviceInfo = require('../controllers/getDeviceInfoController');

    // todoList Routes
    app.route('/getDevicesInfo')
        .get(deviceInfo.list_all_devices_within_organization)
        .post(deviceInfo.create_a_device);


    app.route('/getDevicesInfo/:projectId')
        .get(deviceInfo.list_all_devices_within_project);

};