'use strict';


var mongoose = require('mongoose'),
    DeviceInfo = mongoose.model('devices');

exports.list_all_devices_within_organization = function(req, res) {
    DeviceInfo.find({}, function(err, deviceTask) {
        if (err)
            res.send(err);
        res.json(deviceTask);
    });
};


exports.list_all_devices_within_project = function(req, res) {
    DeviceInfo.findById(req.params.projectId, function(err, deviceTask) {
        if (err)
            res.send(err);
        res.json(deviceTask);
    });
};


exports.create_a_device = function(req, res) {
    var newDevice = new DeviceInfo(req.body);
    newDevice.save(function(err, deviceTask) {
        if (err)
            res.send(err);
        res.json(deviceTask);
    });
};